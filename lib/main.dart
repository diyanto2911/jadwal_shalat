import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:jadwalimsyak/Utils/portraitmode.dart';
import 'package:jadwalimsyak/screen/home.dart';
import 'package:provider/provider.dart';

import 'providers/providerShalat.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget  with PortraitModeMixin{
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return  MultiProvider(
      providers: [
        ChangeNotifierProvider<ProviderShalat>(
          create: (_) => ProviderShalat(),
        ),
      ],
      child: DynamicTheme(
          defaultBrightness: Brightness.light,
          data: (brightness) => new ThemeData(
            primarySwatch: Colors.blue,
            brightness: brightness,
          ),
          themedWidgetBuilder: (context, theme) {
            return MaterialApp(
              title: 'Jadwal Shalat',
              debugShowCheckedModeBanner: false,
              theme: theme,
              home: Home(),
            );
          }
      ),
    );
  }
}

