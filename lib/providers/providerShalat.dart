import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/foundation.dart';
import 'package:jadwalimsyak/models/model_current_shalat.dart';
import 'package:jadwalimsyak/models/model_shalat.dart';

class ProviderShalat extends ChangeNotifier{


  bool _loadingCurrent=true;
  String _messageAPiCurrent;
  int _codeCurrent;


  bool _loading=true;
  String _messageAPi;
  int _code;

  static BaseOptions options = new BaseOptions(
    connectTimeout: 5000,
    receiveTimeout: 3000,
  );
  Dio dio=new Dio(options);

  bool get loadingCurrent => _loadingCurrent;

  set loadingCurrent(bool value) {
    _loadingCurrent = value;
    notifyListeners();
  }

  String get messageAPiCurrent => _messageAPiCurrent;

  set messageAPiCurrent(String value) {
    _messageAPiCurrent = value;
    notifyListeners();
  }

  int get codeCurrent => _codeCurrent;

  set codeCurrent(int value) {
    _codeCurrent = value;
    notifyListeners();
  }


  bool get loading => _loading;

  set loading(bool value) {
    _loading = value;
    notifyListeners();
  }



  String get messageAPi => _messageAPi;

  set messageAPi(String value) {
    _messageAPi = value;
    notifyListeners();
  }

  int get code => _code;

  set code(int value) {
    _code = value;
    notifyListeners();
  }

  ModelCurrentShalat _modelCurrentShalat;

  ModelCurrentShalat get modelCurrentShalat => _modelCurrentShalat;

  set modelCurrentShalat(ModelCurrentShalat value) {
    _modelCurrentShalat = value;
    notifyListeners();
  }

  Future<ModelCurrentShalat> getCurrentShalat({String country,String city})async{
    try{

      dio = new Dio(options);
      Response response;
      String url = "http://api.aladhan.com/v1/timingsByCity";
      response = await dio.get(url, queryParameters: {
        "city" : "$city",
        "country" : "$country",
        "method" : "5"
      });
      if(response.statusCode==200){
        messageAPiCurrent=response.data['status'];
        codeCurrent=response.data['code'];
        modelCurrentShalat= ModelCurrentShalat.fromJson(response.data);

      }
      loadingCurrent=false;



    }on DioError catch(e){
      loadingCurrent=false;
      codeCurrent=404;
    }
    return modelCurrentShalat;


  }

  ModelShalat _modelShalat;

  ModelShalat get modelShalat => _modelShalat;

  set modelShalat(ModelShalat value) {
    _modelShalat = value;
    notifyListeners();
  }

  Future<ModelShalat> getData({String city,String country,int month,String year})async{

    try{

      dio = new Dio(options);
      Response response;
      String url = "http://api.aladhan.com/v1/hijriCalendarByCity";
      response = await dio.get(url, queryParameters: {
        "city" : "$city",
        "country" : "$country",
        "method" : "5",
        "month" : "$month",
        "year" : "$year"
      });
      if(response.statusCode==200){
        messageAPiCurrent=response.data['status'];
        codeCurrent=response.data['code'];
        modelShalat= ModelShalat.fromJson(response.data);

      }
      loadingCurrent=false;



    }on DioError catch(e){
      loadingCurrent=false;
      codeCurrent=404;
    }
    return modelShalat;


  }


}