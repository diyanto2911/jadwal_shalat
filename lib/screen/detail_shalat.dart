import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jadwalimsyak/Utils/sizeConfig.dart';
import 'package:jadwalimsyak/models/model_shalat.dart';

class DetailShalat extends StatefulWidget {
  final DataShalat dataShalat;
  final String imageBackground;

  const DetailShalat({Key key, this.dataShalat, this.imageBackground})
      : super(key: key);

  @override
  _DetailShalatState createState() => _DetailShalatState();
}

class _DetailShalatState extends State<DetailShalat> {
  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: Image.asset(
              "${widget.imageBackground}",
              height: ScreenConfig.blockHorizontal * 58,
              width: ScreenConfig.screenWidth,
              fit: BoxFit.cover,
            ),
          ),
          Positioned(
              top: ScreenConfig.blockHorizontal * 40,
              left: ScreenConfig.blockHorizontal * 5,
              right: ScreenConfig.blockHorizontal * 5,
              child: Card(
                elevation: 4,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Center(
                    child:    Icon(Icons.calendar_today,size: 35)
                      ),
                      SizedBox(height: 10,),
                      Center(
                          child: Text(
                            "${widget.dataShalat.date.gregorian.weekday.en}",
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize:ScreenConfig.blockHorizontal*5,fontWeight: FontWeight.bold),
                          )),
                      SizedBox(height: 5,),
                      Center(
                          child: Text(
                        "${widget.dataShalat.date.hijri.day} ${widget.dataShalat.date.hijri.month.en} ${widget.dataShalat.date.hijri.year}",
                        textAlign: TextAlign.center,
                            style: TextStyle(fontSize:ScreenConfig.blockHorizontal*4,fontWeight: FontWeight.bold),
                      )),
                      Center(
                          child: Text(
                            "${widget.dataShalat.date.gregorian.day} ${widget.dataShalat.date.gregorian.month.en} ${widget.dataShalat.date.gregorian.year}",
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize:ScreenConfig.blockHorizontal*4),
                          ))
                    ],
                  ),
                ),
              )),
          Positioned(
              top: ScreenConfig.blockHorizontal * 77,
              left: ScreenConfig.blockHorizontal * 5,
              right: ScreenConfig.blockHorizontal * 5,
              child: Card(
                elevation: 4,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.room_service,size: 25,),
                        SizedBox(width: 10,),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Imsyak",
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize:ScreenConfig.blockHorizontal*3),
                            ),
                        SizedBox(height: 5,),
                        Text(
                        "${widget.dataShalat.timings.imsak}",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize:ScreenConfig.blockHorizontal*3.6),
                        ),
                          ],
                        )
                      ],
                    ),
                      SizedBox(height: 20,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.room_service,size: 25,),
                          SizedBox(width: 10,),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Subuh",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize:ScreenConfig.blockHorizontal*3),
                              ),
                              SizedBox(height: 5,),
                              Text(
                                "${widget.dataShalat.timings.fajr}",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize:ScreenConfig.blockHorizontal*3.6),
                              ),
                            ],
                          )
                        ],
                      ),
                      SizedBox(height: 20,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.room_service,size: 25,),
                          SizedBox(width: 10,),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Dhuzur",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize:ScreenConfig.blockHorizontal*3),
                              ),
                              SizedBox(height: 5,),
                              Text(
                                "${widget.dataShalat.timings.dhuhr}",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize:ScreenConfig.blockHorizontal*3.6),
                              ),
                            ],
                          ),

                        ],
                      ),
                      SizedBox(height: 20,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.room_service,size: 25,),
                          SizedBox(width: 10,),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Ashar",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize:ScreenConfig.blockHorizontal*3),
                              ),
                              SizedBox(height: 5,),
                              Text(
                                "${widget.dataShalat.timings.asr}",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize:ScreenConfig.blockHorizontal*3.6),
                              ),
                            ],
                          )
                        ],
                      ),
                      SizedBox(height: 20,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.room_service,size: 25,),
                          SizedBox(width: 10,),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Magrib",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize:ScreenConfig.blockHorizontal*3),
                              ),
                              SizedBox(height: 5,),
                              Text(
                                "${widget.dataShalat.timings.maghrib}",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize:ScreenConfig.blockHorizontal*3.6),
                              ),
                            ],
                          )
                        ],
                      ),
                      SizedBox(height: 20,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.room_service,size: 25,),
                          SizedBox(width: 10,),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Isya",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize:ScreenConfig.blockHorizontal*3),
                              ),
                              SizedBox(height: 5,),
                              Text(
                                "${widget.dataShalat.timings.isha}",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize:ScreenConfig.blockHorizontal*3.6),
                              ),
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ))
        ],
      ),
    );
  }
}
