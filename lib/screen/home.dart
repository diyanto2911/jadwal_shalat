import 'dart:async';

import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:jadwalimsyak/Utils/sizeConfig.dart';
import 'package:jadwalimsyak/providers/providerShalat.dart';
import 'package:jadwalimsyak/screen/detail_shalat.dart';
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var location = new Location();
  LatLng _lastMapPosition;

  LatLng latLngDevice = new LatLng(0, 0);

  String lokasi = "";

  String kota;

  var imageBackground = "assets/bg_header_night.png";
  void changeBrightnessDark() {
    DynamicTheme.of(context).setBrightness(
             Brightness.dark);
  }
  void changeBrightness() {
    DynamicTheme.of(context).setBrightness(
        Brightness.light);
  }

  void getLocation() async {
    if (this.mounted) {
      location.onLocationChanged().listen((LocationData currentLocation) {
        try {
          getNameLocation(currentLocation.latitude, currentLocation.longitude);
          print(currentLocation.latitude);
          setState(() {
            latLngDevice =
                new LatLng(currentLocation.latitude, currentLocation.longitude);
          });
          _lastMapPosition =
              new LatLng(currentLocation.latitude, currentLocation.longitude);
        } catch (e) {}
      });
    }

    print(latLngDevice);
  }

  void getNameLocation(double lat, double long) async {
    if (this.mounted) {
      final coordinates = new Coordinates(lat, long);
      try {
        var addresses = await Geocoder.local
            .findAddressesFromCoordinates(coordinates)
            .then((address) {
          setState(() {
            var first = address.first.addressLine;
            lokasi = first;
            kota = address.first.subAdminArea;
            print(kota);
            Provider.of<ProviderShalat>(context, listen: false)
                .getCurrentShalat(
                    city: kota, country: address.first.countryName)
                .then((res) {
              if (res != null) {
                Provider.of<ProviderShalat>(
                  context,
                  listen: false,
                ).getData(
                    city: kota,
                    country: address.first.countryName,
                    year: res.data.date.hijri.year,
                    month: res.data.date.hijri.month.number);
              }
            });
          });
        });
      } on PlatformException catch (e) {}
    }
  }

  void _getTime() {
    final DateTime now = DateTime.now();
    print(now.hour);
    if (now.hour > 0 && now.hour < 6) {
      changeBrightness();
      setState(() {
        imageBackground = "assets/bg_header_dawn.png";
      });
    } else if (now.hour >= 6 && now.hour < 12) {
      setState(() {
        imageBackground = "assets/bg_header_sunrise.png";
      });
    } else if (now.hour >= 12 && now.hour < 16) {
      setState(() {
        imageBackground = "assets/bg_header_daylight.png";
      });
    } else if (now.hour >= 16 && now.hour < 18) {
      changeBrightnessDark();
      setState(() {
        imageBackground = "assets/bg_header_evening.png";
      });
    } else if (now.hour >= 18 && now.hour < 24) {
      changeBrightnessDark();
      setState(() {
        imageBackground = "assets/bg_header_night.png";
      });
    }
  }

  @override
  void initState() {
    getLocation();
    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      body: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: Image.asset(
              "$imageBackground",
              height: ScreenConfig.blockHorizontal * 58,
              width: ScreenConfig.screenWidth,
              fit: BoxFit.cover,
            ),
          ),
          Positioned(
              top: ScreenConfig.blockHorizontal * 18,
              left: 0,
              right: 0,
              child: Consumer<ProviderShalat>(builder: (context, data, _) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Waktu Imsyak Hari ini",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          letterSpacing: 2,
                          fontSize: ScreenConfig.blockHorizontal * 5),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: ScreenConfig.blockHorizontal * 4,
                    ),
                    Text(
                      data.modelCurrentShalat == null
                          ? "memuat.."
                          : data.modelCurrentShalat.data.timings.imsak,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: ScreenConfig.blockHorizontal * 6,
                          color: Colors.white),
                    )
                  ],
                );
              })),
          Positioned(
              top: ScreenConfig.blockHorizontal * 44,
              left: 5,
              right: 5,
              bottom: 0,
              child: Consumer<ProviderShalat>(builder: (context, data, _) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.location_on,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 4,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "$kota",
                                  style: TextStyle(
                                      fontSize:
                                          ScreenConfig.blockHorizontal * 3,
                                      color: Colors.white),
                                ),
                                Container(
                                  width: ScreenConfig.blockHorizontal * 20,
                                  child: Text(
                                    "$lokasi",
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize:
                                            ScreenConfig.blockHorizontal * 3,
                                        color: Colors.white),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.date_range,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 4,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  data.modelCurrentShalat == null
                                      ? "..."
                                      : "${data.modelCurrentShalat.data.date.hijri.day} Ramandhan ${data.modelCurrentShalat.data.date.hijri.date.split("-").last}",
                                  style: TextStyle(
                                      fontSize:
                                          ScreenConfig.blockHorizontal * 3,
                                      color: Colors.white),
                                ),
                                Container(
                                  width: ScreenConfig.blockHorizontal * 20,
                                  child: Text(
                                    data.modelCurrentShalat == null
                                        ? "..."
                                        : "${data.modelCurrentShalat.data.date.gregorian.day} ${data.modelCurrentShalat.data.date.gregorian.month.en} ${data.modelCurrentShalat.data.date.gregorian.year}",
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize:
                                            ScreenConfig.blockHorizontal * 3,
                                        color: Colors.white),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                    Card(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Subuh",
                                    style: TextStyle(
                                        fontSize:
                                            ScreenConfig.blockHorizontal * 3),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    data.modelCurrentShalat == null
                                        ? "..."
                                        : data.modelCurrentShalat.data.timings
                                            .fajr,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize:
                                            ScreenConfig.blockHorizontal * 4),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Dhuzur",
                                    style: TextStyle(
                                        fontSize:
                                            ScreenConfig.blockHorizontal * 3),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    data.modelCurrentShalat == null
                                        ? "..."
                                        : data.modelCurrentShalat.data.timings
                                            .dhuhr,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize:
                                            ScreenConfig.blockHorizontal * 4),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Asyar",
                                    style: TextStyle(
                                        fontSize:
                                            ScreenConfig.blockHorizontal * 3),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    data.modelCurrentShalat == null
                                        ? "..."
                                        : data.modelCurrentShalat.data.timings
                                            .asr,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize:
                                            ScreenConfig.blockHorizontal * 4),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Maghrib",
                                    style: TextStyle(
                                        fontSize:
                                            ScreenConfig.blockHorizontal * 3),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    data.modelCurrentShalat == null
                                        ? "..."
                                        : data.modelCurrentShalat.data.timings
                                            .maghrib,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize:
                                            ScreenConfig.blockHorizontal * 4),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Isya",
                                    style: TextStyle(
                                        fontSize:
                                            ScreenConfig.blockHorizontal * 3),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    data.modelCurrentShalat == null
                                        ? "..."
                                        : data.modelCurrentShalat.data.timings
                                            .isha,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize:
                                            ScreenConfig.blockHorizontal * 4),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                  ],
                );
              })),
          Positioned(
              top: ScreenConfig.blockHorizontal * 67,
              left: 5,
              right: 5,
              bottom: 0,
              child: Consumer<ProviderShalat>(builder: (context, data, _) {
                if (data.modelShalat != null) {
                  return MediaQuery.removePadding(
                      removeTop: true,
                      context: context,
                      child: ListView.builder(
                          itemCount: data.modelShalat.data.length,
                          itemBuilder: (c, i) {
                            return InkWell(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailShalat(dataShalat: data.modelShalat.data[i],imageBackground: imageBackground,)));
                              },
                              child: Card(
                                elevation: 3,
                                child: Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(data.modelShalat.data[i].date.hijri
                                              .day +
                                          " " +
                                          data.modelShalat.data[i].date.hijri
                                              .month.en +
                                          " " +
                                          data.modelShalat.data[i].date.hijri
                                              .year,
                                      style: TextStyle(fontSize: ScreenConfig.blockHorizontal*4.5,fontWeight: FontWeight.bold),),
                                      SizedBox(height: 5,),
                                      Text(data.modelShalat.data[i].date.gregorian
                                          .day +
                                          " " +
                                          data.modelShalat.data[i].date.gregorian
                                              .month.en +
                                          " " +
                                          data.modelShalat.data[i].date.gregorian
                                              .year,
                                        style: TextStyle(fontSize: ScreenConfig.blockHorizontal*4),)
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }));
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              }))
        ],
      ),
    );
  }
}
